// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode');


// this method is called when your extension is activated
// your extension is activated the very first time the command is executed

/**
 * @param {vscode.ExtensionContext} context
 */

function activate(context) {
	const { Board, Piezo, LCD, Sensor } = require('johnny-five');
	const songs = require('j5-songs');

	const workingDirPath = 'C:\\Users\\604270\\workspace\\arduino-vscode-extension';
	const git = require('simple-git')(workingDirPath);



	let board, sensor, piezo, lcd;
	const song = songs.load('mario-intro');
	const song2 = songs.load('mario-fanfare');

	
	const initializeBoardComponents = () => {
		piezo = new Piezo(8);

		lcd = new LCD({
			pins: [12, 11, 5, 4, 3, 2]
		});

		
		sensor = new Sensor({
			pin: "A0",
			freq: 50,
			threshold: 5
		});

		lcd.useChar('check');
		lcd.useChar('arrowne');
		lcd.useChar('arrowse');
		lcd.clear();

		sensor.on("change", function (evt) {
			if (evt >= 1010) {
				lcd.clear();
				lcd.blink().print('Committing...');
				console.log('commiting...');
				git.commit(`Commit at ${new Date()}`, (e) => {
					lcd.cursor(1, 0).noBlink().print("Commit OK");
				})
			} else if (evt < 1010 && evt >= 900) {
				lcd.clear();
				lcd.blink().print(':arrowne: Pushing...');
				console.log('Pushing...');
				git.push('origin', 'johnny_five_version',  (e) => {
					lcd.cursor(1, 0).noBlink().print(":check: Pushed! ");
					piezo.play(song)
				})
			} else if (evt < 900 && evt >= 400) {
				lcd.clear();
				lcd.blink().print(':arrowse: Pulling...');
				console.log('Pulling...');
				git.pull('origin', 'johnny_five_version',  (e) => {
					lcd.cursor(1, 0).noBlink().print(":check: Pulled! ");
					piezo.play(song2)
				})
			}
		});

	}

	const start = () => {
		const timeout = vscode.workspace.getConfiguration('arduino-quick-board').timeout;
		vscode.window.showInformationMessage('Connecting to device...!');
		board = null;
		board = new Board({ repl: false, debug: false, timeout: timeout });
		const connectionTimeout = setTimeout(() => {
			vscode.window.showErrorMessage('Couldn\'t connect to quick board. Please ensure the device is connected and run Start again.');
		}, timeout);

		board.on("ready", () => {
			clearTimeout(connectionTimeout);
			vscode.window.showInformationMessage('Connected!!');
			initializeBoardComponents();
		});

		board.on("close", () => {
			vscode.window.showErrorMessage('Device disconnected.');
			piezo = null;
			lcd = null;

		});

		board.on("fail", () => {
			vscode.window.showErrorMessage('Error with device.');
		});
	}

	// Add VS Code commands
	const startCommand = vscode.commands.registerCommand('arduino-quick-board.start', start)

	context.subscriptions.push(startCommand);
}
exports.activate = activate;

// this method is called when your extension is deactivated
function deactivate() { }

module.exports = {
	activate,
	deactivate
}





