const { Board, Led, Button, Sensor, Piezo } = require("johnny-five");
var songs = require('j5-songs');

const fetch = require('node-fetch');
const board = new Board();

const workingDirPath = 'C:\\Users\\528581\\repos\\arduino';
const git = require('simple-git')(workingDirPath);


/**
 * GITLAB API
 */

const MR_STATUSES = {
  opened: 'opened',
  closed: 'closed',
  locked: 'locked',
  merged: 'merged',
}

let projectId = '15215185';
let mergeRequestId = '1';

const sampleUrl = `https://gitlab.com/api/v4/projects/${projectId}/merge_requests/${mergeRequestId}`;

let mergeRequestStatus = undefined;

board.on("ready", function () {

  var sensor = new Sensor({
    pin: "A0",
    freq: 50,
    threshold: 5
  });
  var piezo = new Piezo(8);
  var song = songs.load('beethovens-fifth');


  const led = new Led.RGB({
    pins: {
      red: 10,
      green: 6,
      blue: 9
    }
  });
  const button = Button(7);

  led.off();
  let isOn = false;



  sensor.on("change", function (evt) {
    // debounce(function () {
    
      if (evt >= 1010) {
        console.log('Button 4');
      } else if (evt < 1010 && evt >= 900) {
        console.log('Button 3');
      } else if (evt < 900 && evt >= 400) {
        console.log('Button 2');
        git.reset([], () => console.log('Reset in soft mode'))
      } else if (evt < 400 && evt>0){
        console.log('Button 1');

        
      fetch(sampleUrl)
      .then((response) => response.json())
      .then(jsonResponse => {
          mergeRequestStatus = jsonResponse.state;
          console.log('hello en then');

          if (process.send) {
            process.send("Hello");
            console.log('hello');
          }

          if (mergeRequestStatus === MR_STATUSES.merged) {
            console.log('success!!')
              led.on();
          } else {
            console.log('error!!')
            led.blink(200);

          }
      });

      }
    
    // }, 500)
  });

  // Add led to REPL (optional)
  board.repl.inject({ led });

  board.repl.inject({
    button: button
});

  // Turn it on and set the initial color





  // "hold" the button is pressed for specified time.
  //        defaults to 500ms (1/2 second)
  //        set
  button.on("hold", function () {
    led.toggle();
    isOn = !isOn;
  });

  button.on("down", function () {
    console.log('Button is down');
    // piezo.play(song);
  });

  button.on("up", function () {
    console.log('Button is up');
    // git.commit(`Commit at ${new Date()}`, (e) => console.log('Commited code. ', e))
    git.add(['.'], (e) => console.log('staged files.', e));
    //git.fetch([], ()=> console.log('fetched from remote'))
    // piezo.play(song);
    git.status( (e)=> console.log('status', e));
  });
});

